FROM wordpress:php8.1-fpm

ENV MEMCACHED_DEPS zlib1g-dev libmemcached-dev libsasl2-dev libpq-dev

# Copying Themes and Plugins into the wordpress image
COPY ["themes","/usr/src/wordpress/wp-content/themes"]
COPY ["plugins","/usr/src/wordpress/wp-content/plugins"]

# Updating the configuration of wordpress image with our own
RUN rm /usr/local/etc/php/conf.d/opcache-recommended.ini
COPY ./config/opcache.ini /usr/local/etc/php/conf.d/opcache.ini
COPY ./config/uploads.ini /usr/local/etc/php/conf.d/uploads.ini
COPY ./config/memcache.ini /usr/local/etc/php/conf.d/memcache.ini
RUN echo "extension=memcached.so" >> /usr/local/etc/php/conf.d/memcached.ini

# install memcached service to use with the cloud
RUN apt-get update

RUN set -xe \
    && apt-get install -y  $MEMCACHED_DEPS \
    && curl -o memcached.tgz -SL http://pecl.php.net/get/memcached-3.2.0.tgz \
    && mkdir -p /usr/src/php/ext/ \ 
    && tar -xf memcached.tgz -C /usr/src/php/ext/ \
    && rm memcached.tgz \
    && mv /usr/src/php/ext/memcached-3.2.0 /usr/src/php/ext/memcached \
    && docker-php-ext-configure memcached --with-php-config=/usr/local/bin/php-config
    
RUN apt-get install --no-install-recommends -y unzip libssl-dev libpcre3 libpcre3-dev && \
    cd /usr/src/php/ext/ && \
    curl -sSL -o php8.zip https://github.com/websupport-sk/pecl-memcache/archive/refs/tags/8.0.1.zip && \
    unzip php8.zip && \
    mv pecl-memcache-8.0.1 memcache && \
    docker-php-ext-configure memcache --enable-memcache --with-php-config=/usr/local/bin/php-config && \
    rm -rf /tmp/pecl-memcache-8.0.1 php8.zip

RUN docker-php-ext-install memcached
RUN docker-php-ext-install memcache

# libonig-dev for mbstring
RUN apt-get install --no-install-recommends -y libonig-dev

RUN docker-php-ext-install mbstring

RUN apt-get install --no-install-recommends -y libxml2-dev
RUN docker-php-ext-install soap

RUN docker-php-ext-enable memcached memcache soap

# cleanup
# cleanup
RUN rm -rf /usr/share/php8 \
    && rm -rf /tmp/* \
    && apt-get remove -y $MEMCACHED_DEPS \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/*
    
# Applying the execution right on the folders for apache
COPY entrypoint-child.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint-child.sh
ENTRYPOINT ["entrypoint-child.sh"]
CMD ["php-fpm"]
