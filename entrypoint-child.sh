#!/bin/bash
set -euo pipefail

exec docker-entrypoint.sh "$@"
